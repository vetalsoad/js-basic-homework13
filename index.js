"use strict";

const images = document.querySelectorAll(".image-to-show");
const stopButton = document.querySelector("#stop-button");
const resumeButton = document.querySelector("#resume-button");
let currentImageIndex = 0;
let timeoutId;

const showNextImage = () => {
  images[currentImageIndex].style.display = "none";
  currentImageIndex = (currentImageIndex + 1) % images.length;
  images[currentImageIndex].style.display = "block";

  timeoutId = setTimeout(showNextImage, 3000);
};

timeoutId = setTimeout(showNextImage, 3000);

stopButton.addEventListener("click", () => {
  clearTimeout(timeoutId);
});

resumeButton.addEventListener("click", () => {
  timeoutId = setTimeout(showNextImage, 3000);
});
